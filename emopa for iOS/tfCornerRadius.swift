//
//  tfCornerRadius.swift
//  
//
//  Created by DHI関西支社 管理者 on 2018/10/01.
//

import UIKit

@IBDesignable class tfCornerRadius: UITextField {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
