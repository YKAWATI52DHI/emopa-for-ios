//
//  1sttutorialViewController.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/11.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class learnerViewController: UIViewController {
    
    @IBOutlet weak var lCollectionView: UICollectionView!
    @IBOutlet weak var lPageControl: UIPageControl!
    @IBOutlet weak var lNext: UIButton!
    
    let UD = UserDefaults.standard
    
    //xibファイルは３つのチュートリアルで使い回ししている
    //Cell内のレイアウトは統一できるからいいかなとは思う
    let reuseIdentifier = "LearnerViewCell"
    
//    let tu = [Playgame(overView: "①アプリの紹介", pict: "logo", text: "晴れてご結婚されたお二人の門出を、参加者の皆さんがゲームに参加してお祝いするアプリです"),
//              Playgame(overView: "②アプリの紹介", pict: "logo", text: "晴れてご結婚されたお二人の門出を、参加者の皆さんがゲームに参加してお祝いするアプリです"),
//              Playgame(overView: "③アプリの紹介", pict: "logo", text: "晴れてご結婚されたお二人の門出を、参加者の皆さんがゲームに参加してお祝いするアプリです")]
    let tu = [Playgame(overView: "", pict: "logo", text: ""),
              Playgame(overView: "", pict: "logo", text: ""),
              Playgame(overView: "", pict: "logo", text: "")]
    //これ長くなるんやったらAssets.xcassets使ったりする方がいいのかな
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPageControl()
        setCollectionView()
        lNext.isHidden = true
    }
    
    
    //ここでもiPhoneX対策というかこれがデフォルトになるのか←全画面表示だからいらない？
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // lCollectionView.frame = view.bounds
    }
    
    
    private func setCollectionView() {
        lCollectionView.register(UINib.init(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        lCollectionView.dataSource = self
        lCollectionView.delegate = self
    }
    
    private func setPageControl() {
        lPageControl.currentPage = 0
        lPageControl.numberOfPages = tu.count
    }
    
    //戻るボタンはない
    @IBAction func nextTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toLI", sender: nil)
    }
    
    //displayMyAlertMessageとはなんぞや
    func displayMyAlertMessage(userMessage: String) {
        let wrongAlert = UIAlertController(title: "エラー", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let itsOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            action in return
        }
        
        wrongAlert.addAction(itsOK)
        self.present(wrongAlert, animated: true, completion: nil)
    }
}

//extension
extension learnerViewController {
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { _ in
            
            self.lCollectionView.collectionViewLayout.invalidateLayout()
            let indexPath = IndexPath(row: self.lPageControl.currentPage, section: 0)
            self.lCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        })
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        targetContentOffset.pointee = targetContentOffsetPoint(scrollView, velocity: velocity)
        
        let x = targetContentOffset.pointee.x
        let index = Int(x / lCollectionView.frame.width)
        lPageControl.currentPage = index
    }
    
}

extension learnerViewController: UICollectionViewDataSource {
   
    func numberOfSections(in collectionView:UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tu.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let lCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! LearnerViewCell
        
        let lItem = tu[indexPath.item]
        
        lCell.configure(Learn: lItem)
        return lCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //最後まで読んだら次へボタンが現れる設定(全てのチュートリアルページで共通)
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if lCollectionView.contentOffset.x + lCollectionView.frame.size.width > lCollectionView.contentSize.width && lCollectionView.isDragging {
            self.lNext.isHidden = false
        }
    }
    
    // スクロールをちょうど良いところで止める補正処理
    // 真ん中を超えたら次へ補正する。
    func targetContentOffsetPoint(_ scrollView: UIScrollView,velocity:CGPoint) -> CGPoint{
        let cellWidth:CGFloat = lCollectionView.bounds.size.width
        let windowWidth:CGFloat = lCollectionView.bounds.size.width
        var offsetAdjustment:CGFloat = CGFloat(MAXFLOAT)
        let horizontalOffest:CGFloat = scrollView.contentOffset.x + ( windowWidth - cellWidth ) / 2
        
        let targetRect = CGRect(x:scrollView.contentOffset.x + velocity.x * cellWidth,
                                y:0,
                                width:lCollectionView.bounds.size.width,
                                height:lCollectionView.bounds.size.height)
        
        let array = lCollectionView.collectionViewLayout.layoutAttributesForElements(in: targetRect)
        
        var bSetFlg = false
        for layoutAttributes in array! {
            let itemOffset = layoutAttributes.frame.origin.x
            if abs(itemOffset - horizontalOffest) < abs(offsetAdjustment) {
                offsetAdjustment = itemOffset - horizontalOffest
                bSetFlg = true
            }
        }
        // 設定しなかった場合。
        if(bSetFlg == false){
            offsetAdjustment=0
        }
        
        // 移動先
        var offsetPosX = scrollView.contentOffset.x + offsetAdjustment
        // 最終を超える場合は最終にする。は最初を超える場合には最初にする
        if(offsetPosX > lCollectionView.contentSize.width){
            offsetPosX = lCollectionView.contentSize.width - lCollectionView.frame.size.width
        }
        else if(offsetPosX < 0){
            offsetPosX = 0
        }
        
        return CGPoint(x:offsetPosX, y:scrollView.contentOffset.y)
    }
    
}


extension learnerViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: lCollectionView.frame.width, height: lCollectionView.frame.height)
    }
}

