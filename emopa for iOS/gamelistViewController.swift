//
//  gamelistViewController.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/05.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class gamelistViewController: UIViewController {
    @IBOutlet weak var glNavigationBar: UINavigationBar!
    @IBOutlet weak var subTitle: UITextView!
    @IBOutlet weak var btnBingo: UIButton!
    @IBOutlet weak var btnGrandprix: UIButton!
    
    let UD = UserDefaults.standard
    
    //subTitle.text = "Welcome to" + (userDefaluts.pName) + "'s party"
    //多分こんな感じでパーティーの名称をブッこむとか
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // subTitle.text = UD.string(forKey: "")
        
        // GAMEの種類
        btnBingo.isHidden = true
        btnGrandprix.isHidden = true
        let arrGamen:[String] = UD.array(forKey: "GAMENS") as! [String]
        for value:String in arrGamen {
            if(value == "1"){
                btnBingo.isHidden = false
            }
            else if(value == "2"){
                btnGrandprix.isHidden = false
            }
        }
    }
    
    
   //keyが通れば動く
    @IBAction func playBingo(_ sender: Any) {
        // D1.ビンゴゲームTOP画面」に遷移
        self.performSegue(withIdentifier: "toBT", sender: nil)
    }
 
    @IBAction func playGrandprix(_ sender: Any) {
        // D1.ビンゴゲームTOP画面」に遷移
        self.performSegue(withIdentifier: "toGT", sender: nil)
    }
    //エラー発生時のダイアログ発生
    func displayMyAlertMessage(userMessage: String) {
        let wrongAlert = UIAlertController(title: "エラー", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let itsOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            action in return
        }
        
        wrongAlert.addAction(itsOK)
        self.present(wrongAlert, animated: true, completion: nil)
    }
    
    @IBAction func unwindFromGame(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        print(unwindSegue.identifier, subsequentVC)
    }

    @IBAction func logout(_ sender: UIButton) {
        // ダイアログの雛形
        let alertController: UIAlertController = UIAlertController(title: "確認", message: "ログアウトしてよろしいですか？", preferredStyle: UIAlertControllerStyle.alert)
        // 左側がキャンセルのようだ
        let cancelAction: UIAlertAction = UIAlertAction(title: "いいえ", style: UIAlertActionStyle.cancel, handler: {(action: UIAlertAction!) -> Void in})
        // ログアウトしまーす
        let logoutAction: UIAlertAction = UIAlertAction(title: "はい", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) -> Void in
            
            // 保持情報クリア
            self.UD.removeObject(forKey: "PARTYCODE")
            self.UD.removeObject(forKey: "PARTYID")
            self.UD.removeObject(forKey: "USERNAME")
            self.UD.removeObject(forKey: "GAMENS")
            
            self.performSegue(withIdentifier: "toLO", sender: nil)
        })
        
        // actionを追加
        alertController.addAction(cancelAction)
        alertController.addAction(logoutAction)
        
        //表示
        present(alertController, animated: true, completion: nil)
    }
}
