//
//  bingoGameViewController.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/10.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit
import WebKit

class bingoGameViewController: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var bWebView: WKWebView!
    
    let UD = UserDefaults.standard
    let GAMEID = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bWebView.scrollView.isScrollEnabled = true //スクロール可能に
        bWebView.scrollView.bounces = false // スクロール時のブラウザのバウンスなし
        
        // URL設定
        let baseUrlString = APIClient.baseUrl()
        let partyCode = self.UD.object(forKey: "PARTYCODE") as! String
        let nickname = self.UD.object(forKey: "USERNAME") as! String
        let partyId = self.UD.object(forKey: "PARTYID") as! String
        
        let urlString = baseUrlString + "/BingoAppli/dispBingoTutorial?paticipantKey=" + partyCode + "," + nickname + "&partyId=" + partyId + "&gameId=" + GAMEID
        let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        
        let url = NSURL(string: encodedUrlString!)
        let request = NSURLRequest(url: url! as URL)
        
        bWebView.load(request as URLRequest)
        
        bWebView.navigationDelegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


