//
//  grandprixtutorialViewController.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/07.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class grandprixtutorialViewController: UIViewController {
    
    @IBOutlet weak var gCollectionView: UICollectionView!
    @IBOutlet weak var gPageControl: UIPageControl!
    @IBOutlet weak var gNext: UIButton!
    
    let reuseIdentifier = "LearnerViewCell"
    
    //ビンゴと中身は変数以外一緒です。
//    let gp = [
//        Playgame(overView:"その１", pict: "logo", text:"グランプリゲームの遊び方"),
//        Playgame(overView: "その２", pict: "logo", text: "様々なお題に合わせてみんながバトルをします。勝者を予想してください。"),
//        Playgame(overView: "その３", pict: "logo", text: "見事予想を的中させたら賞品がもらえます！\nレイアウトはこれでいいですか？")]
    let gp = [Playgame(overView: "", pict: "logo", text: ""),
              Playgame(overView: "", pict: "logo", text: ""),
              Playgame(overView: "", pict: "logo", text: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPageControl()
        setCollectionView()
        gNext.isHidden = true
    }
    
    //ここでもiPhoneX対策というかこれがデフォルトになるのか←全画面表示だからいらない？
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // lCollectionView.frame = view.bounds
    }
    
    private func setCollectionView() {
        gCollectionView.register(UINib.init(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        gCollectionView.dataSource = self
        gCollectionView.delegate = self
    }
    
    private func setPageControl() {
        gPageControl.currentPage = 0
        gPageControl.numberOfPages = gp.count
    }

    @IBAction func nextTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toGG", sender: nil)
    }
}

//extension
extension grandprixtutorialViewController {
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { _ in
            
            self.gCollectionView.collectionViewLayout.invalidateLayout()
            let indexPath = IndexPath(row: self.gPageControl.currentPage, section: 0)
            self.gCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        })
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        targetContentOffset.pointee = targetContentOffsetPoint(scrollView, velocity: velocity)
        
        let x = targetContentOffset.pointee.x
        let index = Int(x / gCollectionView.frame.width)
        gPageControl.currentPage = index
        
    }
}

extension grandprixtutorialViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gp.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let gCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! LearnerViewCell
        
        let gItem = gp[indexPath.item]
        
        gCell.configure(Learn: gItem)
        return gCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if gCollectionView.contentOffset.x + gCollectionView.frame.size.width > gCollectionView.contentSize.width && gCollectionView.isDragging {
            self.gNext.isHidden = false
        }
    }
    
    // スクロールをちょうど良いところで止める補正処理
    // 真ん中を超えたら次へ補正する。
    func targetContentOffsetPoint(_ scrollView: UIScrollView,velocity:CGPoint) -> CGPoint{
        let cellWidth:CGFloat = gCollectionView.bounds.size.width
        let windowWidth:CGFloat = gCollectionView.bounds.size.width
        var offsetAdjustment:CGFloat = CGFloat(MAXFLOAT)
        let horizontalOffest:CGFloat = scrollView.contentOffset.x + ( windowWidth - cellWidth ) / 2
        
        let targetRect = CGRect(x:scrollView.contentOffset.x + velocity.x * cellWidth,
                                y:0,
                                width:gCollectionView.bounds.size.width,
                                height:gCollectionView.bounds.size.height)
        
        let array = gCollectionView.collectionViewLayout.layoutAttributesForElements(in: targetRect)
        
        var bSetFlg = false
        for layoutAttributes in array! {
            let itemOffset = layoutAttributes.frame.origin.x
            if abs(itemOffset - horizontalOffest) < abs(offsetAdjustment) {
                offsetAdjustment = itemOffset - horizontalOffest
                bSetFlg = true
            }
        }
        // 設定しなかった場合。
        if(bSetFlg == false){
            offsetAdjustment=0
        }
        
        // 移動先
        var offsetPosX = scrollView.contentOffset.x + offsetAdjustment
        // 最終を超える場合は最終にする。は最初を超える場合には最初にする
        if(offsetPosX > gCollectionView.contentSize.width){
            offsetPosX = gCollectionView.contentSize.width - gCollectionView.frame.size.width
        }
        else if(offsetPosX < 0){
            offsetPosX = 0
        }
        
        return CGPoint(x:offsetPosX, y:scrollView.contentOffset.y)
    }
}

extension grandprixtutorialViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: gCollectionView.frame.width, height: gCollectionView.frame.height)
    }
}
