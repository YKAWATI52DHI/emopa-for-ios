//
//  Config.swift
//  emopa for iOS
//
//  Created by Satoru Fukuda on 2018/10/17.
//  Copyright © 2018 DHI関西支社 管理者. All rights reserved.
//

import Foundation

class Config {
    static let env = "dev"
    
    enum Env {
        case dev
        case stg
        case prod
    }
    
    static func getEnv() -> Env {
        switch env {
        case "stg":
            return .stg
        case "prod":
            return .prod
        default:
            return .dev
        }
    }
    
    struct Pusher {
        static func appId() -> String {
            switch Config.getEnv() {
            case .dev:
                return "603889"
            case .stg:
                return "603890"
            case .prod:
                return "603891"
            }
        }
        
        static func key() -> String {
            switch Config.getEnv() {
            case .dev:
                return "7c2297b2b53fa3a8df9b"
            case .stg:
                return "da7cbe872eef19310f33"
            case .prod:
                return "8fe3f5aad3e4e27efbad"
            }
        }
        
        static func secret() -> String {
            switch Config.getEnv() {
            case .dev:
                return "b277e9eb1155c1dbe9ea"
            case .stg:
                return "425db768bd9b59fd5ce0"
            case .prod:
                return "17f5bc626bc1c2b01ab8"
            }
        }

        static func cluster() -> String {
            switch Config.getEnv() {
            case .dev:
                return "ap1"
            case .stg:
                return "ap1"
            case .prod:
                return "ap1"
            }
        }
    }
}
