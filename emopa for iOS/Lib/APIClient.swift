//
//  APIClient.swift
//  emopa for iOS
//
//  Created by Satoru Fukuda on 2018/10/17.
//  Copyright © 2018 DHI関西支社 管理者. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIClient {
    
    /***********************
     * APIの認証情報
     ***********************/
    /** APPキー("*** X-Emopa-API-Key ***") */
    static let auth_api_key: String = "d8ba05ff20893f2bfe4d0d5acafb4bab45fe033f6334ed5dc1d1be99d3ff967e"
    /** シークレット情報("*** X-Emopa-API-Secret ***") */
    static let auth_api_secret: String = "9650066999e91559fb1e4db81cc69b51d98a724fb344568eb5912b96a4c1064a"
    
    enum Path: String {
        case userRegist = "AppliManage/registUser"
        case login = "AppliManage/login"
        case endParty = "AppliManage/getEndPartyId"
    }
    
    static func baseUrl() -> String {
        switch Config.getEnv() {
        case .stg:
            return "https://stg.emotionalparty-app.jp/"
        case .prod:
            return "https://prod.emotionalparty-app.jp/"
        default:
            return "http://dev.emotionalparty-app.jp/"
        }
    }
    
    static func get(path: Path, params: Parameters) {
        let url = self.baseUrl() + path.rawValue
        Alamofire.request(url, method: .get, parameters: params)
            .validate {request, response, data in
                return .success
            }
            .responseJSON { response in
                debugPrint(response)
        }
    }
    
    static func post(path: Path, params: Parameters, onSuccess: @escaping (JSON) -> Void, onError: @escaping () -> Void) {
        let url = self.baseUrl() + path.rawValue
        // Debug request.
        print("send request [\(path.rawValue)]:", params)
        
        let headers = [
            "X-Emopa-API-Key": self.auth_api_key,
            "X-Emopa-API-Secret": self.auth_api_secret
        ]
        
        Alamofire.request(url, method: .post, parameters: params, headers: headers)
            .validate { request, response, data in
                return .success
            }
            .responseJSON { response in
                // Debug response.
                print("get response [\(path.rawValue)]:", response)
                
                if response.result.isSuccess {
                    if let value = response.result.value {
                        let json = JSON(value)
                        onSuccess(json)
                    } else {
                        onError()
                    }
                } else {
                    onError()
                }
        }
    }
}
