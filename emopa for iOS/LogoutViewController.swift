//
//  LogoutViewController.swift
//  emopa for iOS
//
//  Created by Satoru Fukuda on 2018/10/26.
//  Copyright © 2018 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class LogoutViewController: UIViewController {
    let UD = UserDefaults.standard
    
    var parties: [String: [String: String]]?
    var partyCodes = [String]()
    
    @IBOutlet weak var vLoginButtons: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if var parties = UD.object(forKey: "PARTIES") as? [String: [String: String]]{
            self.parties = parties
            
            // 再ログイン不可情報取得
            let userId = UD.string(forKey: "USERID")
            
            let params: [String: Any] = [
                "userId" : userId!,
                ]
            APIClient.post(path: .endParty, params: params, onSuccess: { data in
                if data["status"].string == "success" {
                    let arrPartyId:[String]? = data["endPartyId"].arrayObject as? [String]
                    
                    // 再ログイン不可を除く
                    if (arrPartyId != nil){
                        for partyId in arrPartyId! {
                            parties[partyId] = nil
                        }
                        self.UD.set(parties, forKey: "PARTIES")
                    }
                    
                    // 再ログインボタンの作成
                    var i = 0
                    for (_, data) in parties {
                        self.partyCodes.append(data["PARTYID"]!)
                        self.addLoginButton(name: data["PARTYNAME"]!, index: i)
                        i += 1
                    }
                } else {
                    // TODO: エラーアラート表示
                    print(data["message"])
                    self.displayMyAlertMessage(userMessage: data["message"].string!)
                }
            }, onError: {
                // TODO: エラーアラート表示
                print("error occurred")
                self.displayMyAlertMessage(userMessage: "通信エラー")
            })
        }
    }
    
    func addLoginButton(name: String, index: Int) {
        let title = name + "に再ログインする"
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        
        vLoginButtons.addSubview(view)
        view.topAnchor.constraint(equalTo: vLoginButtons.topAnchor, constant: 0).isActive = true
        view.leadingAnchor.constraint(equalTo: vLoginButtons.leadingAnchor, constant: 30).isActive = true
        vLoginButtons.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 30).isActive = true
        view.heightAnchor.constraint(equalToConstant: 50).isActive = true

        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor(red: 69/255, green: 63/255, blue: 137/255, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        button.backgroundColor = UIColor.white
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        button.contentHorizontalAlignment = .left
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0)
        button.addTarget(self, action: #selector(loginParty(sender:)), for: .touchUpInside)
        button.tag = index
        button.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(button)
        
        button.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        button.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: button.trailingAnchor).isActive = true
        
        let img = UIImage(named: "rightArrowBlueCircle")
        let iv = UIImageView(image: img)
        iv.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(iv)
        
        iv.heightAnchor.constraint(equalToConstant: 28).isActive = true
        iv.widthAnchor.constraint(equalToConstant: 28).isActive = true
        iv.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: iv.trailingAnchor, constant: 15).isActive = true
    }
    
    @objc func loginParty(sender: UIButton) {
        let userId = UD.string(forKey: "USERID")
        
        let partyId = partyCodes[sender.tag]
        if let party = self.parties?[partyId] {
            
            let nickname = party["NICKNAME"]
            let partyCode = party["PARTYCODE"]

            let params: [String: Any] = [
                "partyCode": partyCode!,
                "nickname": nickname!,
                "userId" : userId!,
                ]
            APIClient.post(path: .login, params: params, onSuccess: { data in
                if data["status"].string == "success" {
                    let partyId = data["partyId"].string
                    let partyName = data["partyName"].string
                    let arrGamen:[String] = data["registGameId"].arrayObject as! [String]
                    
                    self.UD.set(partyCode, forKey: "PARTYCODE")
                    self.UD.set(nickname, forKey: "USERNAME")
                    self.UD.set(partyId, forKey: "PARTYID")
                    self.UD.set(arrGamen, forKey: "GAMENS")
                    
                    self.parties?[partyId!] = [
                        "NICKNAME": nickname,
                        "PARTYCODE": partyCode,
                        "PARTYID": partyId!,
                        "PARTYNAME": partyName!,
                        ] as? [String : String]
                    
                    self.performSegue(withIdentifier: "toGL", sender: nil)
                } else {
                    // TODO: エラーアラート表示
                    print(data["message"])
                    self.displayMyAlertMessage(userMessage: data["message"].string!)
                }
            }, onError: {
                // TODO: エラーアラート表示
                print("error occurred")
                self.displayMyAlertMessage(userMessage: "通信エラー")
            })
        }
    }
        
    //displayMyAlertMessageとはなんぞや
    func displayMyAlertMessage(userMessage: String) {
        let wrongAlert = UIAlertController(title: "エラー", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let itsOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            action in return
        }
        
        wrongAlert.addAction(itsOK)
        self.present(wrongAlert, animated: true, completion: nil)
    }
}
