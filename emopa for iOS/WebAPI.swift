//
//  WebAPI.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/14.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

public class WebAPI {
    
    private let session: URLSession
    
    public init(config: URLSessionConfiguration? = nil) {
        self.session = config.map {URLSession(configuration: $0)} ?? URLSession.shared
    }
        
        func execute(request: URLRequest) -> (NSData?, URLResponse?, NSError?) {
            var d: NSData? = nil
            var r: URLResponse? = nil
            var e: NSError? = nil
            
            let semaphore = DispatchSemaphore(value: 0)
            session.dataTask(with: request) {(data, response, error) -> Void in
                d =  data as NSData?
                r = response
                e =  error as NSError?
                semaphore.signal()
            }
            .resume()
            _ = semaphore.wait(timeout: DispatchTime.distantFuture)
            return (d, r, e)
        }
}

