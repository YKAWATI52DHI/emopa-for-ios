//
//  loginViewController.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/05.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class loginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var partycodeTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    let UD = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //入れさせた値をUserDefaultsで使うための作業
        partycodeTextField.delegate = self
        nameTextField.delegate = self
        
        let partyCode = UD.string(forKey: "PARTYCODE")
        let userName = UD.string(forKey: "USERNAME")
        partycodeTextField.text = partyCode
        nameTextField.text = userName
        
        // ログイン中なら自動ログインする
        if partyCode != nil && userName != nil && partyCode != "" && userName != "" {
            loginSession(partyCode: partyCode!, nickname: userName!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //キーボードをしまわせる。画面の外をタップすれば大きなキーボードもしまえる。
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //returnでもしまえる
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        
        //入力させる
        let partyCode = partycodeTextField.text
        let name = nameTextField.text
        
        //不備があった時その1 空欄編
        if(partyCode == "" || name == "") {
            displayMyAlertMessage(userMessage: "パーティーコード、お名前を入力してください")
        } else if(partyCode == ""){
            displayMyAlertMessage(userMessage: "パーティーコードを入力してください")
        } else if(name == ""){
            displayMyAlertMessage(userMessage: "お名前を入力してください")
        } else if(name!.count > 16){
            displayMyAlertMessage(userMessage: "お名前は16文字以内で入力してください")
        } else {
            // ログイン処理
            if chkUser() == true {
                loginSession(partyCode: partyCode!, nickname: name!)
            }
        }
    }
    
    func chkUser() -> Bool {
        var bRet: Bool = true
        let userId = UD.string(forKey: "USERID")
        // ユーザーIDが取れていない場合は、取得
        if userId == "" {
            // ユーザー登録
            let params: [String: Any] = [
                :]
            APIClient.post(path: .userRegist, params: params, onSuccess: { data in
                if data["status"].string == "success" {
                    // ユーザーID登録
                    let userId = data["userId"].string
                    self.UD.set(userId, forKey: "USERID")
                    // 初回起動成功を保存
                    self.UD.set(true, forKey: "LAUNCHED")
                } else {
                    // TODO: エラーアラート表示
                    print(data["message"])
                    self.displayMyAlertMessage(userMessage: data["message"].string!)
                    bRet = false
                }
            }, onError: {
                // TODO: エラーアラート表示
                print("error occurred")
                self.displayMyAlertMessage(userMessage: "通信エラー")
                bRet = false
            })
        }
        return bRet
    }
    
    func loginSession(partyCode: String, nickname: String) {
        let userId = UD.string(forKey: "USERID")
        var parties = UD.object(forKey: "PARTIES") as? [String: [String: String]]

        let params: [String: Any] = [
            "partyCode": partyCode,
            "nickname": nickname,
            "userId" : userId!,
        ]
        APIClient.post(path: .login, params: params, onSuccess: { data in
            if data["status"].string == "success" {
                let partyId = data["partyId"].string
                let partyName = data["partyName"].string
                let arrGamen:[String] = data["registGameId"].arrayObject as! [String]
                
                self.UD.set(partyCode, forKey: "PARTYCODE")
                self.UD.set(nickname, forKey: "USERNAME")
                self.UD.set(partyId, forKey: "PARTYID")
                self.UD.set(arrGamen, forKey: "GAMENS")
                
                parties?[partyId!] = [
                    "NICKNAME": nickname,
                    "PARTYCODE": partyCode,
                    "PARTYID": partyId!,
                    "PARTYNAME": partyName!,
                ]
                self.UD.set(parties, forKey: "PARTIES")
                
                self.performSegue(withIdentifier: "toGL", sender: nil)
            } else {
                // TODO: エラーアラート表示
                print(data["message"])
                self.displayMyAlertMessage(userMessage: data["message"].string!)
            }
        }, onError: {
            // TODO: エラーアラート表示
            print("error occurred")
            self.displayMyAlertMessage(userMessage: "通信エラー")
        })
    }

    
    //displayMyAlertMessageとはなんぞや
    func displayMyAlertMessage(userMessage: String) {
        let wrongAlert = UIAlertController(title: "エラー", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let itsOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            action in return
        }
        
        wrongAlert.addAction(itsOK)
        self.present(wrongAlert, animated: true, completion: nil)
    }
    
    
    @IBAction func backAndLogout(segue: UIStoryboardSegue) {
        UD.removeObject(forKey: "PARTYCODE")
        UD.removeObject(forKey: "PARTYID")
        UD.removeObject(forKey: "USERNAME")
        UD.removeObject(forKey: "GAMENS")
    }
}
