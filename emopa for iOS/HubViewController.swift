//
//  TutorialViewController.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/05.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class HubViewController: UIViewController {
    
    let UD = UserDefaults.standard
    var bFirstFlg:Bool = false
    
    override func viewDidLoad() {
        //ロードが終わったらしてほしいことを書いておくところ
        super.viewDidLoad()
        
        // 初回起動時の処理
        bFirstFlg = !UD.bool(forKey: "LAUNCHED")
        if bFirstFlg == true {
            // 初期データ作成
            UD.register(defaults: [
                "USERID": "",       // 発行されるUSERID
                "PARTYCODE": "",    // ログイン中のPARTYCODE
                "USERNAME": "",     // ログイン中のUSERNAME
                "PARTYID": "",    // ログイン中のPARTYID
                "GAMENS": [String](),  // ログイン中のゲーム種
                ])
            UD.set([String: String](), forKey: "PARTIES")
            //NICKNAME : string
            //PARTYNAME :string
            //PARTYCODE : string
            //PARTYID : string
            
            // ユーザー登録
            let params: [String: Any] = [
                :]
            APIClient.post(path: .userRegist, params: params, onSuccess: { data in
                if data["status"].string == "success" {
                    // ユーザーID登録
                    let userId = data["userId"].string
                    self.UD.set(userId, forKey: "USERID")
                    // 初回起動成功を保存
                    self.UD.set(true, forKey: "LAUNCHED")
                } else {
                    // TODO: エラーアラート表示
                    print(data["message"])
                    self.displayMyAlertMessage(userMessage: data["message"].string!)
                    //exit(0)
                }
            }, onError: {
                // TODO: エラーアラート表示
                print("error occurred")
                self.displayMyAlertMessage(userMessage: "通信エラー")
                //exit(0)
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if bFirstFlg == true {
            performSegue(withIdentifier: "SegueLearner", sender: nil)
        }
        else{
            performSegue(withIdentifier: "Segue1", sender: nil)
        }
    }
    
    //displayMyAlertMessageとはなんぞや
    func displayMyAlertMessage(userMessage: String) {
        let wrongAlert = UIAlertController(title: "エラー", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let itsOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            action in return
        }
        
        wrongAlert.addAction(itsOK)
        self.present(wrongAlert, animated: true, completion: nil)
    }
}
