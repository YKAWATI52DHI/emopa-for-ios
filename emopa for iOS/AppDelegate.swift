//
//  AppDelegate.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/05.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //起動判定
        //StoryBoardのインスタンス
        let SB = UIStoryboard(name: "Main", bundle: nil)
        let UD = UserDefaults.standard
        // 初回起動時の処理
        if UD.bool(forKey: "LAUNCHED") == false {
            // 初期データ作成
            UD.register(defaults: [
                "USERID": "",       // 発行されるUSERID
                "PARTYCODE": "",    // ログイン中のPARTYCODE
                "USERNAME": "",     // ログイン中のUSERNAME
                "PARTYID": "",    // ログイン中のPARTYID
                "GAMENS": [String](),  // ログイン中のゲーム種
                ])
            UD.set([String: String](), forKey: "PARTIES")
            //NICKNAME : string
            //PARTYNAME :string
            //PARTYCODE : string
            //PARTYID : string
            
//            //チュートリアルに移動
//            let tutorial = SB.instantiateViewController(withIdentifier: "LE") as! learnerViewController
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            self.window?.rootViewController = tutorial
        }
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        //誤作動でアプリがバックグラウンドに行った時のセッションの保持を指示するのはここ
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //セッション再開はここで！
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //初回ログイン判定のフラグが書き変わるとか、ゲームが終わって使わなくなる時はどうするかとか
    }
}

