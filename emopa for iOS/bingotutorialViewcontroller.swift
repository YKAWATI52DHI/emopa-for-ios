//
//  bingotutorialViewcontroller.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/05.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class bingotutorialViewController: UIViewController {
    
    @IBOutlet weak var bCollectionView: UICollectionView!
    @IBOutlet weak var bPageControl: UIPageControl!
    @IBOutlet weak var bNext: UIButton!
    
    let reuseIdentifier = "LearnerViewCell"
    
    //表示する内容はこの辺にのせておく
//    let bingo = [
//        Playgame(overView: "①ビンゴゲームの流れ", pict: "logo", text: "会場のスクリーンにビンゴの番号が映し出され、アプリ内のビンゴカードが連動して点滅します。\n点滅部分をタップしてビンゴカードに穴をあけてください\nビンゴになるとスクリーンにあなたのお名前が表示されます"),
//        Playgame(overView: "①ビンゴゲームの流れ", pict: "logo", text: "会場のスクリーンにビンゴの番号が映し出され、アプリ内のビンゴカードが連動して点滅します。\n点滅部分をタップしてビンゴカードに穴をあけてください\nビンゴになるとスクリーンにあなたのお名前が表示されます"),
//        Playgame(overView: "①ビンゴゲームの流れ", pict: "logo", text: "会場のスクリーンにビンゴの番号が映し出され、アプリ内のビンゴカードが連動して点滅します。\n点滅部分をタップしてビンゴカードに穴をあけてください\nビンゴになるとスクリーンにあなたのお名前が表示されます")
//    ]
    let bingo = [Playgame(overView: "", pict: "logo", text: ""),
              Playgame(overView: "", pict: "logo", text: ""),
              Playgame(overView: "", pict: "logo", text: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPageControl()
        setCollectionView()
        
        bNext.isHidden = true
    }
    
    //ここでもiPhoneX対策というかこれがデフォルトになるのか←全画面表示だからいらない？
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // lCollectionView.frame = view.bounds
    }
    
    private func setCollectionView() {
        bCollectionView.register(UINib.init(nibName: reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        bCollectionView.dataSource = self
        bCollectionView.delegate = self
    }
    
    private func setPageControl() {
        bPageControl.currentPage = 0
        bPageControl.numberOfPages = bingo.count
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        // D2-0-３.ビンゴカード選択」画面に遷移
        self.performSegue(withIdentifier: "toBG", sender: "nil")
    }

    //アラート用メソッド
    func displayMyAlertMessage(userMessage: String) {
        let wrongAlert = UIAlertController(title: "エラー", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        let itsOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default){
            action in return
        }
        
        wrongAlert.addAction(itsOK)
        self.present(wrongAlert, animated: true, completion: nil)
    }
}

//UIViewControllerしか継承してないのでいろんなものをつけたし
extension bingotutorialViewController {
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { _ in
            
            self.bCollectionView.collectionViewLayout.invalidateLayout()
            let indexPath = IndexPath(row: self.bPageControl.currentPage, section: 0)
            self.bCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            
        })
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        targetContentOffset.pointee = targetContentOffsetPoint(scrollView, velocity: velocity)
        
        let x = targetContentOffset.pointee.x
        let index = Int(x / bCollectionView.frame.width)
        bPageControl.currentPage = index
    }
}

extension bingotutorialViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bingo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! LearnerViewCell
        
        let bItem = bingo[indexPath.item]
        
        bCell.configure(Learn: bItem)
        return bCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //最後まできたら次へボタンを出すための命令
        if bCollectionView.contentOffset.x + bCollectionView.frame.size.width > bCollectionView.contentSize.width && bCollectionView.isDragging {
            self.bNext.isHidden = false
        }
    }
    
    // スクロールをちょうど良いところで止める補正処理
    // 真ん中を超えたら次へ補正する。
    func targetContentOffsetPoint(_ scrollView: UIScrollView,velocity:CGPoint) -> CGPoint{
        let cellWidth:CGFloat = bCollectionView.bounds.size.width
        let windowWidth:CGFloat = bCollectionView.bounds.size.width
        var offsetAdjustment:CGFloat = CGFloat(MAXFLOAT)
        let horizontalOffest:CGFloat = scrollView.contentOffset.x + ( windowWidth - cellWidth ) / 2
        
        let targetRect = CGRect(x:scrollView.contentOffset.x + velocity.x * cellWidth,
                                y:0,
                                width:bCollectionView.bounds.size.width,
                                height:bCollectionView.bounds.size.height)
        
        let array = bCollectionView.collectionViewLayout.layoutAttributesForElements(in: targetRect)
        
        var bSetFlg = false
        for layoutAttributes in array! {
            let itemOffset = layoutAttributes.frame.origin.x
            if abs(itemOffset - horizontalOffest) < abs(offsetAdjustment) {
                offsetAdjustment = itemOffset - horizontalOffest
                bSetFlg = true
            }
        }
        // 設定しなかった場合。
        if(bSetFlg == false){
            offsetAdjustment=0
        }
        
        // 移動先
        var offsetPosX = scrollView.contentOffset.x + offsetAdjustment
        // 最終を超える場合は最終にする。は最初を超える場合には最初にする
        if(offsetPosX > bCollectionView.contentSize.width){
            offsetPosX = bCollectionView.contentSize.width - bCollectionView.frame.size.width
        }
        else if(offsetPosX < 0){
            offsetPosX = 0
        }
        
        return CGPoint(x:offsetPosX, y:scrollView.contentOffset.y)
    }
}

extension bingotutorialViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: bCollectionView.frame.width, height: bCollectionView.frame.height)
    }
}


