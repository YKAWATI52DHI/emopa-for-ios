//
//  Playgame.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/05.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import Foundation

struct Playgame {
    let overView: String
    let pict: String
    let text: String
}
