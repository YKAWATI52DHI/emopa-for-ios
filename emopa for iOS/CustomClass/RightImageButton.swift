//
//  RightImageButton.swift
//  emopa for iOS
//
//  Created by Satoru Fukuda on 2018/10/26.
//  Copyright © 2018 DHI関西支社 管理者. All rights reserved.
//

import UIKit

@IBDesignable final class RightImageButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    private func setup() {
        transform = CGAffineTransform(scaleX: -1, y: 1)
        titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
        imageView?.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
}
