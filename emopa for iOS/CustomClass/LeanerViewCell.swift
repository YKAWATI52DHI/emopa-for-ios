//
//  LeanerViewCell.swift
//  emopa for iOS
//
//  Created by Satoru Fukuda on 2018/10/25.
//  Copyright © 2018 DHI関西支社 管理者. All rights reserved.
//

import UIKit

final class LearnerViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tvDetail: UITextView!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(Learn: Playgame) {
        lblTitle.text = Learn.overView
        tvDetail.text = Learn.text
        
        if(Learn.overView == ""){
            lblTitle.isHidden = true
        }
        if(Learn.text == ""){
            tvDetail.isHidden = true
        }
        
        if(Learn.pict == ""){
            imgView.isHidden = true
        }
        else{
            let image:UIImage? = UIImage(named:Learn.pict)
            // Optional Bindingでnilチェック
            if let validImage = image {
                imgView.image = validImage
            } else {
                // 画像がなかった場合の処理
                imgView.isHidden = true
            }
        }
    }
}
