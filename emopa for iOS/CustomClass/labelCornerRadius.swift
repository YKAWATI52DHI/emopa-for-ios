//
//  labelCornerRadius.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/10/01.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

@IBDesignable class labelCornerRadius: UILabel {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
}
