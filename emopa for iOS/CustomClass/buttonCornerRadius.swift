//
//  buttonCornerRadius.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/10/01.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

@IBDesignable
class buttonCornerRadius: UIButton {
    
    @IBInspectable var textColor: UIColor?
    
    @IBInspectable var cornerRadius : CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
}
