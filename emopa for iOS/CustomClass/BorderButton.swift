//
//  BorderButton.swift
//  emopa for iOS
//
//  Created by Satoru Fukuda on 2018/10/25.
//  Copyright © 2018 DHI関西支社 管理者. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
}
