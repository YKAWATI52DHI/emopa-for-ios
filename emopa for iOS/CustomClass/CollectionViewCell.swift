//
//  CollectionViewCell.swift
//  emopa for iOS
//
//  Created by DHI関西支社 管理者 on 2018/09/07.
//  Copyright © 2018年 DHI関西支社 管理者. All rights reserved.
//

import UIKit

final class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var pageImage: UIImageView!
    @IBOutlet weak var detailText: UITextView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(playbingo: Playgame) {
        
        pageTitle.text = playbingo.overView
        
        pageImage.image = UIImage(named: playbingo.pict)
        
        detailText.text = playbingo.text
    }
    
    func configure2(playGP: Playgame) {
        
        pageTitle.text = playGP.overView
        
        pageImage.image = UIImage(named: playGP.pict)
        
        detailText.text = playGP.text
    }
    
    func configure3(Learn: Playgame) {
        
        pageTitle.text = Learn.overView
        pageImage.image = UIImage(named: Learn.pict)
        detailText.text = Learn.text
    }
    

}
